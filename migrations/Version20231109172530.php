<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231109172530 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE table_accident_voitures (id INT AUTO_INCREMENT NOT NULL, type_accident VARCHAR(255) DEFAULT NULL, date_actident DATE DEFAULT NULL, lieu_accident VARCHAR(255) DEFAULT NULL, description_accident LONGTEXT DEFAULT NULL, photo_accident VARCHAR(255) DEFAULT NULL, photo_apres_reparation VARCHAR(255) DEFAULT NULL, assurance_accident VARCHAR(255) DEFAULT NULL, constat_accident VARCHAR(255) DEFAULT NULL, dedomagement_accident VARCHAR(255) DEFAULT NULL, coup_reparation VARCHAR(255) DEFAULT NULL, date_remise_en_circulation DATE DEFAULT NULL, cle_contrat INT DEFAULT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_by VARCHAR(255) DEFAULT NULL, modified_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', modified_by VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_assureurs (id INT AUTO_INCREMENT NOT NULL, enseigne VARCHAR(255) NOT NULL, agence VARCHAR(255) DEFAULT NULL, adresse VARCHAR(255) DEFAULT NULL, contacte VARCHAR(255) DEFAULT NULL, telephone VARCHAR(15) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, cle_voitures INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_couleur_voitures (id INT AUTO_INCREMENT NOT NULL, couleur_vehicule VARCHAR(255) DEFAULT NULL, cle_voitures INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_images (id INT AUTO_INCREMENT NOT NULL, image_de VARCHAR(255) DEFAULT NULL, nom_image VARCHAR(255) DEFAULT NULL, chemin VARCHAR(255) DEFAULT NULL, cle_voitures INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_marque_voitures (id INT AUTO_INCREMENT NOT NULL, marques VARCHAR(100) NOT NULL, cle_voitures INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_modele_voitures (id INT AUTO_INCREMENT NOT NULL, modele_vehicule VARCHAR(255) DEFAULT NULL, cle_voitures INT DEFAULT NULL, cle_marque INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_modified_by (id INT AUTO_INCREMENT NOT NULL, table_relier VARCHAR(100) NOT NULL, cle_from_table INT NOT NULL, modified_by VARCHAR(50) NOT NULL, modified_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_nettoyage_voitures (id INT AUTO_INCREMENT NOT NULL, type_nettoyage VARCHAR(255) DEFAULT NULL, prestataire VARCHAR(255) DEFAULT NULL, cle_prestataire INT DEFAULT NULL, date_nettoyage DATE DEFAULT NULL, adresse_nettoyage VARCHAR(255) DEFAULT NULL, coup_nettoyage VARCHAR(20) DEFAULT NULL, cle_voitures INT DEFAULT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_by VARCHAR(20) DEFAULT NULL, modified_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', modified_by VARCHAR(20) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_prestataire (id INT AUTO_INCREMENT NOT NULL, enseigne VARCHAR(255) DEFAULT NULL, adresse VARCHAR(255) DEFAULT NULL, contacte VARCHAR(255) DEFAULT NULL, telephone VARCHAR(15) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, cle_voitures INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_reparation_voitures (id INT AUTO_INCREMENT NOT NULL, type_reparation VARCHAR(255) DEFAULT NULL, reparateur VARCHAR(255) DEFAULT NULL, adresse_reparation VARCHAR(255) DEFAULT NULL, date_entree DATE DEFAULT NULL, date_sortie DATE DEFAULT NULL, coup_reparation VARCHAR(20) DEFAULT NULL, piece_changer VARCHAR(255) DEFAULT NULL, description_reparation LONGTEXT DEFAULT NULL, cle_voitures INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_type_assurances (id INT AUTO_INCREMENT NOT NULL, type_assurance VARCHAR(255) DEFAULT NULL, cle_voitures INT DEFAULT NULL, cle_assureur INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_type_voitures (id INT AUTO_INCREMENT NOT NULL, type_vehicule VARCHAR(20) DEFAULT NULL, cle_voitures INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_vidange (id INT AUTO_INCREMENT NOT NULL, kilometrage VARCHAR(20) DEFAULT NULL, date_entree DATE DEFAULT NULL, date_sortie DATE DEFAULT NULL, coup_vidange VARCHAR(20) DEFAULT NULL, huile_used VARCHAR(255) DEFAULT NULL, cle_prestataire INT DEFAULT NULL, cle_voitures INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_voitures (id INT AUTO_INCREMENT NOT NULL, type_vehicule VARCHAR(20) DEFAULT NULL, marque_vehicule VARCHAR(100) DEFAULT NULL, modele_vehicule VARCHAR(100) DEFAULT NULL, cv_fiscale_vehicule VARCHAR(100) DEFAULT NULL, num_chasse_vehicule VARCHAR(255) DEFAULT NULL, martricule_vehicule VARCHAR(20) DEFAULT NULL, nbr_places_vehicule VARCHAR(5) DEFAULT NULL, nbr_de_porte_vehicule VARCHAR(5) DEFAULT NULL, couleur_vehicule VARCHAR(255) DEFAULT NULL, prix_achat_vehicule VARCHAR(20) DEFAULT NULL, valeur_actuelle_vehicule VARCHAR(20) DEFAULT NULL, assureur_vehicule VARCHAR(255) DEFAULT NULL, typee_assurance_vehicule VARCHAR(255) DEFAULT NULL, duree_assurance_vehicule VARCHAR(20) DEFAULT NULL, assurance_started_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', assurance_ended_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', copie_carte_grise_vehicule VARCHAR(255) DEFAULT NULL, copie_assurance_vehicule VARCHAR(255) DEFAULT NULL, copie_visite_vehicule VARCHAR(255) DEFAULT NULL, photo_voiture_vehicule VARCHAR(255) DEFAULT NULL, prix_vente_vehicule VARCHAR(20) DEFAULT NULL, nbr_accident_vehicule VARCHAR(5) DEFAULT NULL, reparation_vehicule VARCHAR(255) DEFAULT NULL, kilometrage_vehicule VARCHAR(20) DEFAULT NULL, vidange_vehicule VARCHAR(255) DEFAULT NULL, nettoyage_vehicule VARCHAR(255) DEFAULT NULL, entretien_vehicule VARCHAR(255) DEFAULT NULL, chaine_distribution_vehicule VARCHAR(255) DEFAULT NULL, filt_air_vehicule VARCHAR(255) DEFAULT NULL, filt_huile_vehicule VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_by VARCHAR(20) DEFAULT NULL, modified_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', modified_by VARCHAR(20) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE table_accident_voitures');
        $this->addSql('DROP TABLE table_assureurs');
        $this->addSql('DROP TABLE table_couleur_voitures');
        $this->addSql('DROP TABLE table_images');
        $this->addSql('DROP TABLE table_marque_voitures');
        $this->addSql('DROP TABLE table_modele_voitures');
        $this->addSql('DROP TABLE table_modified_by');
        $this->addSql('DROP TABLE table_nettoyage_voitures');
        $this->addSql('DROP TABLE table_prestataire');
        $this->addSql('DROP TABLE table_reparation_voitures');
        $this->addSql('DROP TABLE table_type_assurances');
        $this->addSql('DROP TABLE table_type_voitures');
        $this->addSql('DROP TABLE table_vidange');
        $this->addSql('DROP TABLE table_voitures');
    }
}
