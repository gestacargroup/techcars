<?php

namespace App\Entity;

use App\Repository\TableTypeAssurancesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableTypeAssurancesRepository::class)]
class TableTypeAssurances
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $type_assurance = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_voitures = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_assureur = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeAssurance(): ?string
    {
        return $this->type_assurance;
    }

    public function setTypeAssurance(?string $type_assurance): static
    {
        $this->type_assurance = $type_assurance;

        return $this;
    }

    public function getCleVoitures(): ?int
    {
        return $this->cle_voitures;
    }

    public function setCleVoitures(?int $cle_voitures): static
    {
        $this->cle_voitures = $cle_voitures;

        return $this;
    }

    public function getCleAssureur(): ?int
    {
        return $this->cle_assureur;
    }

    public function setCleAssureur(?int $cle_assureur): static
    {
        $this->cle_assureur = $cle_assureur;

        return $this;
    }
}
