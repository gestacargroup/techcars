<?php

namespace App\Entity;

use App\Repository\TableReparationVoituresRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableReparationVoituresRepository::class)]
class TableReparationVoitures
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $type_reparation = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $reparateur = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $adresse_reparation = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $date_entree = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $date_sortie = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $coup_reparation = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $piece_changer = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description_reparation = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_voitures = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeReparation(): ?string
    {
        return $this->type_reparation;
    }

    public function setTypeReparation(?string $type_reparation): static
    {
        $this->type_reparation = $type_reparation;

        return $this;
    }

    public function getReparateur(): ?string
    {
        return $this->reparateur;
    }

    public function setReparateur(?string $reparateur): static
    {
        $this->reparateur = $reparateur;

        return $this;
    }

    public function getAdresseReparation(): ?string
    {
        return $this->adresse_reparation;
    }

    public function setAdresseReparation(?string $adresse_reparation): static
    {
        $this->adresse_reparation = $adresse_reparation;

        return $this;
    }

    public function getDateEntree(): ?\DateTimeInterface
    {
        return $this->date_entree;
    }

    public function setDateEntree(?\DateTimeInterface $date_entree): static
    {
        $this->date_entree = $date_entree;

        return $this;
    }

    public function getDateSortie(): ?\DateTimeInterface
    {
        return $this->date_sortie;
    }

    public function setDateSortie(?\DateTimeInterface $date_sortie): static
    {
        $this->date_sortie = $date_sortie;

        return $this;
    }

    public function getCoupReparation(): ?string
    {
        return $this->coup_reparation;
    }

    public function setCoupReparation(?string $coup_reparation): static
    {
        $this->coup_reparation = $coup_reparation;

        return $this;
    }

    public function getPieceChanger(): ?string
    {
        return $this->piece_changer;
    }

    public function setPieceChanger(?string $piece_changer): static
    {
        $this->piece_changer = $piece_changer;

        return $this;
    }

    public function getDescriptionReparation(): ?string
    {
        return $this->description_reparation;
    }

    public function setDescriptionReparation(?string $description_reparation): static
    {
        $this->description_reparation = $description_reparation;

        return $this;
    }

    public function getCleVoitures(): ?int
    {
        return $this->cle_voitures;
    }

    public function setCleVoitures(?int $cle_voitures): static
    {
        $this->cle_voitures = $cle_voitures;

        return $this;
    }
}
