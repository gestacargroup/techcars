<?php

namespace App\Entity;

use App\Repository\TableNettoyageVoituresRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableNettoyageVoituresRepository::class)]
class TableNettoyageVoitures
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $type_nettoyage = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $prestataire = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_prestataire = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $date_nettoyage = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $adresse_nettoyage = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $coup_nettoyage = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_voitures = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $created_by = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $modified_at = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $modified_by = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeNettoyage(): ?string
    {
        return $this->type_nettoyage;
    }

    public function setTypeNettoyage(?string $type_nettoyage): static
    {
        $this->type_nettoyage = $type_nettoyage;

        return $this;
    }

    public function getPrestataire(): ?string
    {
        return $this->prestataire;
    }

    public function setPrestataire(?string $prestataire): static
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    public function getClePrestataire(): ?int
    {
        return $this->cle_prestataire;
    }

    public function setClePrestataire(?int $cle_prestataire): static
    {
        $this->cle_prestataire = $cle_prestataire;

        return $this;
    }

    public function getDateNettoyage(): ?\DateTimeInterface
    {
        return $this->date_nettoyage;
    }

    public function setDateNettoyage(?\DateTimeInterface $date_nettoyage): static
    {
        $this->date_nettoyage = $date_nettoyage;

        return $this;
    }

    public function getAdresseNettoyage(): ?string
    {
        return $this->adresse_nettoyage;
    }

    public function setAdresseNettoyage(?string $adresse_nettoyage): static
    {
        $this->adresse_nettoyage = $adresse_nettoyage;

        return $this;
    }

    public function getCoupNettoyage(): ?string
    {
        return $this->coup_nettoyage;
    }

    public function setCoupNettoyage(?string $coup_nettoyage): static
    {
        $this->coup_nettoyage = $coup_nettoyage;

        return $this;
    }

    public function getCleVoitures(): ?int
    {
        return $this->cle_voitures;
    }

    public function setCleVoitures(?int $cle_voitures): static
    {
        $this->cle_voitures = $cle_voitures;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeImmutable $created_at): static
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->created_by;
    }

    public function setCreatedBy(?string $created_by): static
    {
        $this->created_by = $created_by;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeImmutable
    {
        return $this->modified_at;
    }

    public function setModifiedAt(?\DateTimeImmutable $modified_at): static
    {
        $this->modified_at = $modified_at;

        return $this;
    }

    public function getModifiedBy(): ?string
    {
        return $this->modified_by;
    }

    public function setModifiedBy(?string $modified_by): static
    {
        $this->modified_by = $modified_by;

        return $this;
    }
}
