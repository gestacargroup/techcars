<?php

namespace App\Entity;

use App\Repository\TableMarqueVoituresRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableMarqueVoituresRepository::class)]
class TableMarqueVoitures
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $marques = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_voitures = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMarques(): ?string
    {
        return $this->marques;
    }

    public function setMarques(string $marques): static
    {
        $this->marques = $marques;

        return $this;
    }

    public function getCleVoitures(): ?int
    {
        return $this->cle_voitures;
    }

    public function setCleVoitures(?int $cle_voitures): static
    {
        $this->cle_voitures = $cle_voitures;

        return $this;
    }
}
