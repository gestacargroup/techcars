<?php

namespace App\Entity;

use App\Repository\TableClientContactRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableClientContactRepository::class)]
class TableClientContact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nomContact = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $prenomcontact = null;

    #[ORM\Column(length: 15, nullable: true)]
    private ?string $telephoneContact = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $emailContact = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomContact(): ?string
    {
        return $this->nomContact;
    }

    public function setNomContact(?string $nomContact): static
    {
        $this->nomContact = $nomContact;

        return $this;
    }

    public function getPrenomcontact(): ?string
    {
        return $this->prenomcontact;
    }

    public function setPrenomcontact(?string $prenomcontact): static
    {
        $this->prenomcontact = $prenomcontact;

        return $this;
    }

    public function getTelephoneContact(): ?string
    {
        return $this->telephoneContact;
    }

    public function setTelephoneContact(?string $telephoneContact): static
    {
        $this->telephoneContact = $telephoneContact;

        return $this;
    }

    public function getEmailContact(): ?string
    {
        return $this->emailContact;
    }

    public function setEmailContact(?string $emailContact): static
    {
        $this->emailContact = $emailContact;

        return $this;
    }
}
