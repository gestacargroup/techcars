<?php

namespace App\Entity;

use App\Repository\TableVoituresRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableVoituresRepository::class)]
class TableVoitures
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $type_vehicule = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $marque_vehicule = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $modele_vehicule = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $cv_fiscale_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $num_chasse_vehicule = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $martricule_vehicule = null;

    #[ORM\Column(length: 5, nullable: true)]
    private ?string $nbr_places_vehicule = null;

    #[ORM\Column(length: 5, nullable: true)]
    private ?string $Nbr_de_porte_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $couleur_vehicule = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $prix_achat_vehicule = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $valeur_actuelle_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $assureur_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $typee_assurance_vehicule = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $duree_assurance_vehicule = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $assurance_started_at = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $assurance_ended_at = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $copie_carte_grise_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $copie_assurance_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $copie_visite_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $photo_voiture_vehicule = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $prix_vente_vehicule = null;

    #[ORM\Column(length: 5, nullable: true)]
    private ?string $nbr_accident_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $reparation_vehicule = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $kilometrage_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $vidange_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nettoyage_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $entretien_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $chaine_distribution_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $filt_air_vehicule = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $filt_huile_vehicule = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $created_by = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $modified_at = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $modified_by = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeVehicule(): ?string
    {
        return $this->type_vehicule;
    }

    public function setTypeVehicule(?string $type_vehicule): static
    {
        $this->type_vehicule = $type_vehicule;

        return $this;
    }

    public function getMarqueVehicule(): ?string
    {
        return $this->marque_vehicule;
    }

    public function setMarqueVehicule(?string $marque_vehicule): static
    {
        $this->marque_vehicule = $marque_vehicule;

        return $this;
    }

    public function getModeleVehicule(): ?string
    {
        return $this->modele_vehicule;
    }

    public function setModeleVehicule(?string $modele_vehicule): static
    {
        $this->modele_vehicule = $modele_vehicule;

        return $this;
    }

    public function getCvFiscaleVehicule(): ?string
    {
        return $this->cv_fiscale_vehicule;
    }

    public function setCvFiscaleVehicule(?string $cv_fiscale_vehicule): static
    {
        $this->cv_fiscale_vehicule = $cv_fiscale_vehicule;

        return $this;
    }

    public function getNumChasseVehicule(): ?string
    {
        return $this->num_chasse_vehicule;
    }

    public function setNumChasseVehicule(?string $num_chasse_vehicule): static
    {
        $this->num_chasse_vehicule = $num_chasse_vehicule;

        return $this;
    }

    public function getMartriculeVehicule(): ?string
    {
        return $this->martricule_vehicule;
    }

    public function setMartriculeVehicule(?string $martricule_vehicule): static
    {
        $this->martricule_vehicule = $martricule_vehicule;

        return $this;
    }

    public function getNbrPlacesVehicule(): ?string
    {
        return $this->nbr_places_vehicule;
    }

    public function setNbrPlacesVehicule(?string $nbr_places_vehicule): static
    {
        $this->nbr_places_vehicule = $nbr_places_vehicule;

        return $this;
    }

    public function getNbrDePorteVehicule(): ?string
    {
        return $this->Nbr_de_porte_vehicule;
    }

    public function setNbrDePorteVehicule(?string $Nbr_de_porte_vehicule): static
    {
        $this->Nbr_de_porte_vehicule = $Nbr_de_porte_vehicule;

        return $this;
    }

    public function getCouleurVehicule(): ?string
    {
        return $this->couleur_vehicule;
    }

    public function setCouleurVehicule(?string $couleur_vehicule): static
    {
        $this->couleur_vehicule = $couleur_vehicule;

        return $this;
    }

    public function getPrixAchatVehicule(): ?string
    {
        return $this->prix_achat_vehicule;
    }

    public function setPrixAchatVehicule(?string $prix_achat_vehicule): static
    {
        $this->prix_achat_vehicule = $prix_achat_vehicule;

        return $this;
    }

    public function getValeurActuelleVehicule(): ?string
    {
        return $this->valeur_actuelle_vehicule;
    }

    public function setValeurActuelleVehicule(?string $valeur_actuelle_vehicule): static
    {
        $this->valeur_actuelle_vehicule = $valeur_actuelle_vehicule;

        return $this;
    }

    public function getAssureurVehicule(): ?string
    {
        return $this->assureur_vehicule;
    }

    public function setAssureurVehicule(?string $assureur_vehicule): static
    {
        $this->assureur_vehicule = $assureur_vehicule;

        return $this;
    }

    public function getTypeeAssuranceVehicule(): ?string
    {
        return $this->typee_assurance_vehicule;
    }

    public function setTypeeAssuranceVehicule(?string $typee_assurance_vehicule): static
    {
        $this->typee_assurance_vehicule = $typee_assurance_vehicule;

        return $this;
    }

    public function getDureeAssuranceVehicule(): ?string
    {
        return $this->duree_assurance_vehicule;
    }

    public function setDureeAssuranceVehicule(?string $duree_assurance_vehicule): static
    {
        $this->duree_assurance_vehicule = $duree_assurance_vehicule;

        return $this;
    }

    public function getAssuranceStartedAt(): ?\DateTimeImmutable
    {
        return $this->assurance_started_at;
    }

    public function setAssuranceStartedAt(?\DateTimeImmutable $assurance_started_at): static
    {
        $this->assurance_started_at = $assurance_started_at;

        return $this;
    }

    public function getAssuranceEndedAt(): ?\DateTimeImmutable
    {
        return $this->assurance_ended_at;
    }

    public function setAssuranceEndedAt(?\DateTimeImmutable $assurance_ended_at): static
    {
        $this->assurance_ended_at = $assurance_ended_at;

        return $this;
    }

    public function getCopieCarteGriseVehicule(): ?string
    {
        return $this->copie_carte_grise_vehicule;
    }

    public function setCopieCarteGriseVehicule(?string $copie_carte_grise_vehicule): static
    {
        $this->copie_carte_grise_vehicule = $copie_carte_grise_vehicule;

        return $this;
    }

    public function getCopieAssuranceVehicule(): ?string
    {
        return $this->copie_assurance_vehicule;
    }

    public function setCopieAssuranceVehicule(?string $copie_assurance_vehicule): static
    {
        $this->copie_assurance_vehicule = $copie_assurance_vehicule;

        return $this;
    }

    public function getCopieVisiteVehicule(): ?string
    {
        return $this->copie_visite_vehicule;
    }

    public function setCopieVisiteVehicule(?string $copie_visite_vehicule): static
    {
        $this->copie_visite_vehicule = $copie_visite_vehicule;

        return $this;
    }

    public function getPhotoVoitureVehicule(): ?string
    {
        return $this->photo_voiture_vehicule;
    }

    public function setPhotoVoitureVehicule(?string $photo_voiture_vehicule): static
    {
        $this->photo_voiture_vehicule = $photo_voiture_vehicule;

        return $this;
    }

    public function getPrixVenteVehicule(): ?string
    {
        return $this->prix_vente_vehicule;
    }

    public function setPrixVenteVehicule(?string $prix_vente_vehicule): static
    {
        $this->prix_vente_vehicule = $prix_vente_vehicule;

        return $this;
    }

    public function getNbrAccidentVehicule(): ?string
    {
        return $this->nbr_accident_vehicule;
    }

    public function setNbrAccidentVehicule(?string $nbr_accident_vehicule): static
    {
        $this->nbr_accident_vehicule = $nbr_accident_vehicule;

        return $this;
    }

    public function getReparationVehicule(): ?string
    {
        return $this->reparation_vehicule;
    }

    public function setReparationVehicule(?string $reparation_vehicule): static
    {
        $this->reparation_vehicule = $reparation_vehicule;

        return $this;
    }

    public function getKilometrageVehicule(): ?string
    {
        return $this->kilometrage_vehicule;
    }

    public function setKilometrageVehicule(?string $kilometrage_vehicule): static
    {
        $this->kilometrage_vehicule = $kilometrage_vehicule;

        return $this;
    }

    public function getVidangeVehicule(): ?string
    {
        return $this->vidange_vehicule;
    }

    public function setVidangeVehicule(?string $vidange_vehicule): static
    {
        $this->vidange_vehicule = $vidange_vehicule;

        return $this;
    }

    public function getNettoyageVehicule(): ?string
    {
        return $this->nettoyage_vehicule;
    }

    public function setNettoyageVehicule(?string $nettoyage_vehicule): static
    {
        $this->nettoyage_vehicule = $nettoyage_vehicule;

        return $this;
    }

    public function getEntretienVehicule(): ?string
    {
        return $this->entretien_vehicule;
    }

    public function setEntretienVehicule(?string $entretien_vehicule): static
    {
        $this->entretien_vehicule = $entretien_vehicule;

        return $this;
    }

    public function getChaineDistributionVehicule(): ?string
    {
        return $this->chaine_distribution_vehicule;
    }

    public function setChaineDistributionVehicule(?string $chaine_distribution_vehicule): static
    {
        $this->chaine_distribution_vehicule = $chaine_distribution_vehicule;

        return $this;
    }

    public function getFiltAirVehicule(): ?string
    {
        return $this->filt_air_vehicule;
    }

    public function setFiltAirVehicule(?string $filt_air_vehicule): static
    {
        $this->filt_air_vehicule = $filt_air_vehicule;

        return $this;
    }

    public function getFiltHuileVehicule(): ?string
    {
        return $this->filt_huile_vehicule;
    }

    public function setFiltHuileVehicule(?string $filt_huile_vehicule): static
    {
        $this->filt_huile_vehicule = $filt_huile_vehicule;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeImmutable $created_at): static
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->created_by;
    }

    public function setCreatedBy(?string $created_by): static
    {
        $this->created_by = $created_by;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeImmutable
    {
        return $this->modified_at;
    }

    public function setModifiedAt(?\DateTimeImmutable $modified_at): static
    {
        $this->modified_at = $modified_at;

        return $this;
    }

    public function getModifiedBy(): ?string
    {
        return $this->modified_by;
    }

    public function setModifiedBy(?string $modified_by): static
    {
        $this->modified_by = $modified_by;

        return $this;
    }
}
