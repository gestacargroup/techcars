<?php

namespace App\Entity;

use App\Repository\CivRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CivRepository::class)]
class Civ
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 10)]
    private ?string $civ = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCiv(): ?string
    {
        return $this->civ;
    }

    public function setCiv(string $civ): static
    {
        $this->civ = $civ;

        return $this;
    }
}
