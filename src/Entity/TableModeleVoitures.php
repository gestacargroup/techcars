<?php

namespace App\Entity;

use App\Repository\TableModeleVoituresRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableModeleVoituresRepository::class)]
class TableModeleVoitures
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $modele_vehicule = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_voitures = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_marque = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModeleVehicule(): ?string
    {
        return $this->modele_vehicule;
    }

    public function setModeleVehicule(?string $modele_vehicule): static
    {
        $this->modele_vehicule = $modele_vehicule;

        return $this;
    }

    public function getCleVoitures(): ?int
    {
        return $this->cle_voitures;
    }

    public function setCleVoitures(?int $cle_voitures): static
    {
        $this->cle_voitures = $cle_voitures;

        return $this;
    }

    public function getCleMarque(): ?int
    {
        return $this->cle_marque;
    }

    public function setCleMarque(?int $cle_marque): static
    {
        $this->cle_marque = $cle_marque;

        return $this;
    }
}
