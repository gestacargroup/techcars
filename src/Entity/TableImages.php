<?php

namespace App\Entity;

use App\Repository\TableImagesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableImagesRepository::class)]
class TableImages
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $image_de = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nom_image = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $chemin = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_voitures = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImageDe(): ?string
    {
        return $this->image_de;
    }

    public function setImageDe(?string $image_de): static
    {
        $this->image_de = $image_de;

        return $this;
    }

    public function getNomImage(): ?string
    {
        return $this->nom_image;
    }

    public function setNomImage(?string $nom_image): static
    {
        $this->nom_image = $nom_image;

        return $this;
    }

    public function getChemin(): ?string
    {
        return $this->chemin;
    }

    public function setChemin(?string $chemin): static
    {
        $this->chemin = $chemin;

        return $this;
    }

    public function getCleVoitures(): ?int
    {
        return $this->cle_voitures;
    }

    public function setCleVoitures(?int $cle_voitures): static
    {
        $this->cle_voitures = $cle_voitures;

        return $this;
    }
}
