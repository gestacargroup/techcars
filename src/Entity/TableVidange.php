<?php

namespace App\Entity;

use App\Repository\TableVidangeRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableVidangeRepository::class)]
class TableVidange
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $kilometrage = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $date_entree = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $date_sortie = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $coup_vidange = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $huile_used = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_prestataire = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_voitures = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKilometrage(): ?string
    {
        return $this->kilometrage;
    }

    public function setKilometrage(?string $kilometrage): static
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    public function getDateEntree(): ?\DateTimeInterface
    {
        return $this->date_entree;
    }

    public function setDateEntree(?\DateTimeInterface $date_entree): static
    {
        $this->date_entree = $date_entree;

        return $this;
    }

    public function getDateSortie(): ?\DateTimeInterface
    {
        return $this->date_sortie;
    }

    public function setDateSortie(?\DateTimeInterface $date_sortie): static
    {
        $this->date_sortie = $date_sortie;

        return $this;
    }

    public function getCoupVidange(): ?string
    {
        return $this->coup_vidange;
    }

    public function setCoupVidange(?string $coup_vidange): static
    {
        $this->coup_vidange = $coup_vidange;

        return $this;
    }

    public function getHuileUsed(): ?string
    {
        return $this->huile_used;
    }

    public function setHuileUsed(?string $huile_used): static
    {
        $this->huile_used = $huile_used;

        return $this;
    }

    public function getClePrestataire(): ?int
    {
        return $this->cle_prestataire;
    }

    public function setClePrestataire(?int $cle_prestataire): static
    {
        $this->cle_prestataire = $cle_prestataire;

        return $this;
    }

    public function getCleVoitures(): ?int
    {
        return $this->cle_voitures;
    }

    public function setCleVoitures(?int $cle_voitures): static
    {
        $this->cle_voitures = $cle_voitures;

        return $this;
    }
}
