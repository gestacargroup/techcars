<?php

namespace App\Entity;

use App\Repository\TableMenuBarRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableMenuBarRepository::class)]
class TableMenuBar
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $home = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $rent = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $finance = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $documents = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $cars = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $contrats = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $setup = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $language = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $Users = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHome(): ?string
    {
        return $this->home;
    }

    public function setHome(?string $home): static
    {
        $this->home = $home;

        return $this;
    }

    public function getRent(): ?string
    {
        return $this->rent;
    }

    public function setRent(?string $rent): static
    {
        $this->rent = $rent;

        return $this;
    }

    public function getFinance(): ?string
    {
        return $this->finance;
    }

    public function setFinance(?string $finance): static
    {
        $this->finance = $finance;

        return $this;
    }

    public function getDocuments(): ?string
    {
        return $this->documents;
    }

    public function setDocuments(?string $documents): static
    {
        $this->documents = $documents;

        return $this;
    }

    public function getCars(): ?string
    {
        return $this->cars;
    }

    public function setCars(?string $cars): static
    {
        $this->cars = $cars;

        return $this;
    }

    public function getContrats(): ?string
    {
        return $this->contrats;
    }

    public function setContrats(?string $contrats): static
    {
        $this->contrats = $contrats;

        return $this;
    }

    public function getSetup(): ?string
    {
        return $this->setup;
    }

    public function setSetup(?string $setup): static
    {
        $this->setup = $setup;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): static
    {
        $this->language = $language;

        return $this;
    }

    public function getUsers(): ?string
    {
        return $this->Users;
    }

    public function setUsers(?string $Users): static
    {
        $this->Users = $Users;

        return $this;
    }
}
