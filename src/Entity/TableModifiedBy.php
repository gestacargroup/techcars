<?php

namespace App\Entity;

use App\Repository\TableModifiedByRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableModifiedByRepository::class)]
class TableModifiedBy
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $table_relier = null;

    #[ORM\Column]
    private ?int $cle_from_table = null;

    #[ORM\Column(length: 50)]
    private ?string $modified_by = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $modified_at = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTableRelier(): ?string
    {
        return $this->table_relier;
    }

    public function setTableRelier(string $table_relier): static
    {
        $this->table_relier = $table_relier;

        return $this;
    }

    public function getCleFromTable(): ?int
    {
        return $this->cle_from_table;
    }

    public function setCleFromTable(int $cle_from_table): static
    {
        $this->cle_from_table = $cle_from_table;

        return $this;
    }

    public function getModifiedBy(): ?string
    {
        return $this->modified_by;
    }

    public function setModifiedBy(string $modified_by): static
    {
        $this->modified_by = $modified_by;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeImmutable
    {
        return $this->modified_at;
    }

    public function setModifiedAt(\DateTimeImmutable $modified_at): static
    {
        $this->modified_at = $modified_at;

        return $this;
    }
}
