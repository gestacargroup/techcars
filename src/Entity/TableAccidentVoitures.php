<?php

namespace App\Entity;

use App\Repository\TableAccidentVoituresRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableAccidentVoituresRepository::class)]
class TableAccidentVoitures
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $type_accident = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $date_actident = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $lieu_accident = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description_accident = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $photo_accident = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $photo_apres_reparation = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $assurance_accident = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $constat_accident = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $dedomagement_accident = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $coup_reparation = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $date_remise_en_circulation = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_contrat = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $created_by = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $modified_at = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $modified_by = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeAccident(): ?string
    {
        return $this->type_accident;
    }

    public function setTypeAccident(?string $type_accident): static
    {
        $this->type_accident = $type_accident;

        return $this;
    }

    public function getDateActident(): ?\DateTimeInterface
    {
        return $this->date_actident;
    }

    public function setDateActident(?\DateTimeInterface $date_actident): static
    {
        $this->date_actident = $date_actident;

        return $this;
    }

    public function getLieuAccident(): ?string
    {
        return $this->lieu_accident;
    }

    public function setLieuAccident(?string $lieu_accident): static
    {
        $this->lieu_accident = $lieu_accident;

        return $this;
    }

    public function getDescriptionAccident(): ?string
    {
        return $this->description_accident;
    }

    public function setDescriptionAccident(?string $description_accident): static
    {
        $this->description_accident = $description_accident;

        return $this;
    }

    public function getPhotoAccident(): ?string
    {
        return $this->photo_accident;
    }

    public function setPhotoAccident(?string $photo_accident): static
    {
        $this->photo_accident = $photo_accident;

        return $this;
    }

    public function getPhotoApresReparation(): ?string
    {
        return $this->photo_apres_reparation;
    }

    public function setPhotoApresReparation(?string $photo_apres_reparation): static
    {
        $this->photo_apres_reparation = $photo_apres_reparation;

        return $this;
    }

    public function getAssuranceAccident(): ?string
    {
        return $this->assurance_accident;
    }

    public function setAssuranceAccident(?string $assurance_accident): static
    {
        $this->assurance_accident = $assurance_accident;

        return $this;
    }

    public function getConstatAccident(): ?string
    {
        return $this->constat_accident;
    }

    public function setConstatAccident(?string $constat_accident): static
    {
        $this->constat_accident = $constat_accident;

        return $this;
    }

    public function getDedomagementAccident(): ?string
    {
        return $this->dedomagement_accident;
    }

    public function setDedomagementAccident(?string $dedomagement_accident): static
    {
        $this->dedomagement_accident = $dedomagement_accident;

        return $this;
    }

    public function getCoupReparation(): ?string
    {
        return $this->coup_reparation;
    }

    public function setCoupReparation(?string $coup_reparation): static
    {
        $this->coup_reparation = $coup_reparation;

        return $this;
    }

    public function getDateRemiseEnCirculation(): ?\DateTimeInterface
    {
        return $this->date_remise_en_circulation;
    }

    public function setDateRemiseEnCirculation(?\DateTimeInterface $date_remise_en_circulation): static
    {
        $this->date_remise_en_circulation = $date_remise_en_circulation;

        return $this;
    }

    public function getCleContrat(): ?int
    {
        return $this->cle_contrat;
    }

    public function setCleContrat(?int $cle_contrat): static
    {
        $this->cle_contrat = $cle_contrat;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeImmutable $created_at): static
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->created_by;
    }

    public function setCreatedBy(?string $created_by): static
    {
        $this->created_by = $created_by;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeImmutable
    {
        return $this->modified_at;
    }

    public function setModifiedAt(?\DateTimeImmutable $modified_at): static
    {
        $this->modified_at = $modified_at;

        return $this;
    }

    public function getModifiedBy(): ?string
    {
        return $this->modified_by;
    }

    public function setModifiedBy(?string $modified_by): static
    {
        $this->modified_by = $modified_by;

        return $this;
    }
}
