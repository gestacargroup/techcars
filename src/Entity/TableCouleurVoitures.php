<?php

namespace App\Entity;

use App\Repository\TableCouleurVoituresRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableCouleurVoituresRepository::class)]
class TableCouleurVoitures
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $couleur_vehicule = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_voitures = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCouleurVehicule(): ?string
    {
        return $this->couleur_vehicule;
    }

    public function setCouleurVehicule(?string $couleur_vehicule): static
    {
        $this->couleur_vehicule = $couleur_vehicule;

        return $this;
    }

    public function getCleVoitures(): ?int
    {
        return $this->cle_voitures;
    }

    public function setCleVoitures(?int $cle_voitures): static
    {
        $this->cle_voitures = $cle_voitures;

        return $this;
    }
}
