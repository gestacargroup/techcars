<?php

namespace App\Entity;

use App\Repository\TableTypeVoituresRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableTypeVoituresRepository::class)]
class TableTypeVoitures
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $type_vehicule = null;

    #[ORM\Column(nullable: true)]
    private ?int $cle_voitures = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeVehicule(): ?string
    {
        return $this->type_vehicule;
    }

    public function setTypeVehicule(?string $type_vehicule): static
    {
        $this->type_vehicule = $type_vehicule;

        return $this;
    }

    public function getCleVoitures(): ?int
    {
        return $this->cle_voitures;
    }

    public function setCleVoitures(?int $cle_voitures): static
    {
        $this->cle_voitures = $cle_voitures;

        return $this;
    }
}
