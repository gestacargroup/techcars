<?php

namespace App\Repository;

use App\Entity\TableClientContact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableClientContact>
 *
 * @method TableClientContact|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableClientContact|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableClientContact[]    findAll()
 * @method TableClientContact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableClientContactRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableClientContact::class);
    }

//    /**
//     * @return TableClientContact[] Returns an array of TableClientContact objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableClientContact
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
