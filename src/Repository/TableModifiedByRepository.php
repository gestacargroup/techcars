<?php

namespace App\Repository;

use App\Entity\TableModifiedBy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableModifiedBy>
 *
 * @method TableModifiedBy|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableModifiedBy|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableModifiedBy[]    findAll()
 * @method TableModifiedBy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableModifiedByRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableModifiedBy::class);
    }

//    /**
//     * @return TableModifiedBy[] Returns an array of TableModifiedBy objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableModifiedBy
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
