<?php

namespace App\Repository;

use App\Entity\TableAssureurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableAssureurs>
 *
 * @method TableAssureurs|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableAssureurs|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableAssureurs[]    findAll()
 * @method TableAssureurs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableAssureursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableAssureurs::class);
    }

//    /**
//     * @return TableAssureurs[] Returns an array of TableAssureurs objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableAssureurs
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
