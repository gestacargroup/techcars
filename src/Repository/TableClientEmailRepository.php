<?php

namespace App\Repository;

use App\Entity\TableClientEmail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableClientEmail>
 *
 * @method TableClientEmail|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableClientEmail|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableClientEmail[]    findAll()
 * @method TableClientEmail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableClientEmailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableClientEmail::class);
    }

//    /**
//     * @return TableClientEmail[] Returns an array of TableClientEmail objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableClientEmail
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
