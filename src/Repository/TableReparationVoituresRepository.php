<?php

namespace App\Repository;

use App\Entity\TableReparationVoitures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableReparationVoitures>
 *
 * @method TableReparationVoitures|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableReparationVoitures|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableReparationVoitures[]    findAll()
 * @method TableReparationVoitures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableReparationVoituresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableReparationVoitures::class);
    }

//    /**
//     * @return TableReparationVoitures[] Returns an array of TableReparationVoitures objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableReparationVoitures
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
