<?php

namespace App\Repository;

use App\Entity\TableAccidentVoitures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableAccidentVoitures>
 *
 * @method TableAccidentVoitures|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableAccidentVoitures|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableAccidentVoitures[]    findAll()
 * @method TableAccidentVoitures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableAccidentVoituresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableAccidentVoitures::class);
    }

//    /**
//     * @return TableAccidentVoitures[] Returns an array of TableAccidentVoitures objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableAccidentVoitures
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
