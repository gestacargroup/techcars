<?php

namespace App\Repository;

use App\Entity\TableCouleurVoitures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableCouleurVoitures>
 *
 * @method TableCouleurVoitures|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableCouleurVoitures|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableCouleurVoitures[]    findAll()
 * @method TableCouleurVoitures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableCouleurVoituresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableCouleurVoitures::class);
    }

//    /**
//     * @return TableCouleurVoitures[] Returns an array of TableCouleurVoitures objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableCouleurVoitures
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
