<?php

namespace App\Repository;

use App\Entity\TableVidange;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableVidange>
 *
 * @method TableVidange|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableVidange|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableVidange[]    findAll()
 * @method TableVidange[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableVidangeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableVidange::class);
    }

//    /**
//     * @return TableVidange[] Returns an array of TableVidange objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableVidange
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
