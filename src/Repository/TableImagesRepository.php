<?php

namespace App\Repository;

use App\Entity\TableImages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableImages>
 *
 * @method TableImages|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableImages|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableImages[]    findAll()
 * @method TableImages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableImagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableImages::class);
    }

//    /**
//     * @return TableImages[] Returns an array of TableImages objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableImages
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
