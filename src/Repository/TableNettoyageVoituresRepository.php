<?php

namespace App\Repository;

use App\Entity\TableNettoyageVoitures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableNettoyageVoitures>
 *
 * @method TableNettoyageVoitures|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableNettoyageVoitures|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableNettoyageVoitures[]    findAll()
 * @method TableNettoyageVoitures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableNettoyageVoituresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableNettoyageVoitures::class);
    }

//    /**
//     * @return TableNettoyageVoitures[] Returns an array of TableNettoyageVoitures objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableNettoyageVoitures
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
