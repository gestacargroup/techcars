<?php

namespace App\Repository;

use App\Entity\TablePrestataire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TablePrestataire>
 *
 * @method TablePrestataire|null find($id, $lockMode = null, $lockVersion = null)
 * @method TablePrestataire|null findOneBy(array $criteria, array $orderBy = null)
 * @method TablePrestataire[]    findAll()
 * @method TablePrestataire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TablePrestataireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TablePrestataire::class);
    }

//    /**
//     * @return TablePrestataire[] Returns an array of TablePrestataire objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TablePrestataire
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
