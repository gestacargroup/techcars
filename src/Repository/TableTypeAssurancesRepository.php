<?php

namespace App\Repository;

use App\Entity\TableTypeAssurances;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableTypeAssurances>
 *
 * @method TableTypeAssurances|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableTypeAssurances|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableTypeAssurances[]    findAll()
 * @method TableTypeAssurances[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableTypeAssurancesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableTypeAssurances::class);
    }

//    /**
//     * @return TableTypeAssurances[] Returns an array of TableTypeAssurances objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableTypeAssurances
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
