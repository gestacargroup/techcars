<?php

namespace App\Repository;

use App\Entity\TableMenuBar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableMenuBar>
 *
 * @method TableMenuBar|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableMenuBar|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableMenuBar[]    findAll()
 * @method TableMenuBar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableMenuBarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableMenuBar::class);
    }

//    /**
//     * @return TableMenuBar[] Returns an array of TableMenuBar objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableMenuBar
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
