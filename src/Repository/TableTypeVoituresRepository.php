<?php

namespace App\Repository;

use App\Entity\TableTypeVoitures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableTypeVoitures>
 *
 * @method TableTypeVoitures|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableTypeVoitures|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableTypeVoitures[]    findAll()
 * @method TableTypeVoitures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableTypeVoituresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableTypeVoitures::class);
    }

//    /**
//     * @return TableTypeVoitures[] Returns an array of TableTypeVoitures objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableTypeVoitures
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
