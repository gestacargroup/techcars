<?php

namespace App\Repository;

use App\Entity\TableMarqueVoitures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableMarqueVoitures>
 *
 * @method TableMarqueVoitures|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableMarqueVoitures|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableMarqueVoitures[]    findAll()
 * @method TableMarqueVoitures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableMarqueVoituresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableMarqueVoitures::class);
    }

//    /**
//     * @return TableMarqueVoitures[] Returns an array of TableMarqueVoitures objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableMarqueVoitures
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
