<?php

namespace App\Repository;

use App\Entity\TableClients;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableClients>
 *
 * @method TableClients|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableClients|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableClients[]    findAll()
 * @method TableClients[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableClientsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableClients::class);
    }

//    /**
//     * @return TableClients[] Returns an array of TableClients objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableClients
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
