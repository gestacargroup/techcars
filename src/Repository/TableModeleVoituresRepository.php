<?php

namespace App\Repository;

use App\Entity\TableModeleVoitures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableModeleVoitures>
 *
 * @method TableModeleVoitures|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableModeleVoitures|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableModeleVoitures[]    findAll()
 * @method TableModeleVoitures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableModeleVoituresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableModeleVoitures::class);
    }

//    /**
//     * @return TableModeleVoitures[] Returns an array of TableModeleVoitures objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableModeleVoitures
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
