<?php

namespace App\Repository;

use App\Entity\TableVoitures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TableVoitures>
 *
 * @method TableVoitures|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableVoitures|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableVoitures[]    findAll()
 * @method TableVoitures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableVoituresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TableVoitures::class);
    }

//    /**
//     * @return TableVoitures[] Returns an array of TableVoitures objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TableVoitures
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
