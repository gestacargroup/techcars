<?php

namespace App\Components;

use App\Entity\TableVoitures;
use App\Entity\TableModeleVoitures;
use App\Entity\TableTypeVoitures;
use App\Entity\TableModifiedBy;
use App\Repository\TableVoituresRepository;
use Doctrine\DBAL\Schema\Table;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('datatables')]
class DatatablesComponent
{
    // public string $title;
    // public string $description;
    // public Table $content;
    public array $Voitures = [
        ['TypeVehicule' => 'berlinemanuel' ,'MarqueVehicule'=> 'volswagen','ModeleVehicule'=> 'golf',
         'CvFiscaleVehicule'=> '6','NumChasseVehicule'=>'AbcdE112255dd236ds48sfsf45d4f5d45f4d5g45qqw','MartriculeVehicule'=>'77 tu 1965'],
        ['TypeVehicule' => 'tourisme' ,'MarqueVehicule'=> 'peugeot','ModeleVehicule'=> '306',
         'CvFiscaleVehicule'=> '7','NumChasseVehicule'=>'AbcdE112255dd236ds48sfsf45d4f5d45f4d5g45qqw','MartriculeVehicule'=>'220 tu 1584'],                  
    ];
    // public function __construct(
    //     private TableVoituresRepository $tableVoituresRepository
    //     ) 
    // {
    //     //public string $title;
    // }

    // public function getVoitures(): TableVoitures    
    // {
    //     return $this->tableVoituresRepository->find(1);
    // }
}
