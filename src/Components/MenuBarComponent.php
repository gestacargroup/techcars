<?php

namespace App\Components;

use App\Entity\TableMenuBar;
use App\Repository\TableMenuBarRepository;
use App\Repository\TableMenuBarTopRepository;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('menubar')]
class MenuBarComponent
{

    public function __construct(
        private TableMenuBarRepository $topmenurepository
    ){
        
    }

    public function getMenubar(): TableMenuBar
    {
        return $this->topmenurepository->find(1);
    }      
}