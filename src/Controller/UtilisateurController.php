<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UtilisateurController extends AbstractController
{
    #[Route('/utilisateur', name: 'app_utilisateur')]
    public function index(): Response
    {
        return $this->render('utilisateur/utilisateurs.html.twig', [
            'controller_name' => 'UtilisateurController',
            'title'=> 'je suis un titre qui vient du controller',
            'content'=> 'je suis un centenue qui vient du controller',
            'message'=> 'je suis le message du controller',
            'textcouleur' => 'danger',
            'bgcouleur' => 'danger',
        ]);
    }
}
