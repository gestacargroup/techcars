<?php

namespace App\Controller;

use App\Repository\TableVoituresRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VoituresController extends AbstractController
{
    #[Route('/voitures', name: 'app_voitures')]
    public function index(TableVoituresRepository $voituresrepo): Response
    {
        $voitures = $voituresrepo->findAll();
        return $this->render('voitures/voitures.html.twig', [
            'controller_name' => 'VoituresController',
            'title'=> 'je suis un titre qui vient du controller',
            'description'   => 'ceci est une description',
            'content'=> 'je suis un centenue qui vient du controller',
            'message'=> 'je suis le message du controller',
            'textcouleur' => 'danger',
            'bgcouleur' => 'danger',
            'Voitures'=> $voitures,
        ]);
    }
}
